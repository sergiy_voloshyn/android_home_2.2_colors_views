package com.xyz.android_home_22;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;


/*
2. Расположить на экране 4 view (это могут быть как контейнеры, так и обычные View).
По клику на любой из них менять цвет 3х других view (view.setBackgroundColor(Color.parseColor("#fffff"));).
 Цвета должны быть заданы в списке (примерно 10 штук). После смены цвета не должно оказаться одинаковых view.

 */
public class MainActivity extends AppCompatActivity {


    View view1;
    View view2;
    View view3;
    View view4;

    int currentColor = 0;
    ArrayList<Integer> colors;
    ArrayList<Integer> fillColors;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        view1 = findViewById(R.id.view1);
        view2 = findViewById(R.id.view2);
        view3 = findViewById(R.id.view3);
        view4 = findViewById(R.id.view4);

        colors = new ArrayList<Integer>();
        colors.add(Color.RED);
        colors.add(Color.GRAY);
        colors.add(Color.CYAN);
        colors.add(Color.MAGENTA);
        colors.add(Color.YELLOW);
        colors.add(Color.BLUE);
        colors.add(Color.GREEN);
        colors.add(Color.DKGRAY);
        colors.add(Color.WHITE);
        colors.add(Color.parseColor("#43675323"));
        colors.add(Color.BLACK);
        colors.add(Color.parseColor("#FF345623"));


        fillColors = new ArrayList<>(4);


        view1.setBackgroundColor(Color.RED);
        fillColors.add(Color.RED);
        view2.setBackgroundColor(Color.MAGENTA);
        fillColors.add(Color.MAGENTA);
        view3.setBackgroundColor(Color.GREEN);
        fillColors.add(Color.GREEN);
        view4.setBackgroundColor(Color.YELLOW);
        fillColors.add(Color.YELLOW);


        view1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Drawable background = view1.getBackground();

                if (background instanceof ColorDrawable) {
                    currentColor = ((ColorDrawable) background).getColor();
                }

                for (int i = 0; i < fillColors.size(); i++) {

                    int tempFillColor = (int) fillColors.get(i);

                    int i1 = 0;
                    while (i1 < colors.size()) {
                        int tempColors = (int) colors.get(i1);
                        if (tempFillColor != tempColors & !fillColors.contains(tempColors) & tempColors != currentColor) {
                            fillColors.set(i, tempColors);
                        }
                        i1++;
                    }
                }
                view2.setBackgroundColor(fillColors.get(0));
                view3.setBackgroundColor(fillColors.get(1));
                view4.setBackgroundColor(fillColors.get(2));
            }
        });


        view2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Drawable background = view2.getBackground();

                if (background instanceof ColorDrawable) {
                    currentColor = ((ColorDrawable) background).getColor();
                }

                for (int i = 0; i < fillColors.size(); i++) {

                    int tempFillColor = (int) fillColors.get(i);

                    int i1 = 0;
                    while (i1 < colors.size()) {
                        int tempColors = (int) colors.get(i1);
                        if (tempFillColor != tempColors & !fillColors.contains(tempColors) & tempColors != currentColor) {
                            fillColors.set(i, tempColors);
                        }
                        i1++;
                    }
                }
                view1.setBackgroundColor(fillColors.get(3));
                view3.setBackgroundColor(fillColors.get(2));
                view4.setBackgroundColor(fillColors.get(1));
            }
        });

        view3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Drawable background = view3.getBackground();

                if (background instanceof ColorDrawable) {
                    currentColor = ((ColorDrawable) background).getColor();
                }

                for (int i = 0; i < fillColors.size(); i++) {

                    int tempFillColor = (int) fillColors.get(i);

                    int i1 = 0;
                    while (i1 < colors.size()) {
                        int tempColors = (int) colors.get(i1);
                        if (tempFillColor != tempColors & !fillColors.contains(tempColors) & tempColors != currentColor) {
                            fillColors.set(i, tempColors);
                        }
                        i1++;
                    }
                }
                view1.setBackgroundColor(fillColors.get(0));
                view2.setBackgroundColor(fillColors.get(2));
                view4.setBackgroundColor(fillColors.get(1));
            }
        });

     view4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Drawable background = view4.getBackground();

                if (background instanceof ColorDrawable) {
                    currentColor = ((ColorDrawable) background).getColor();
                }

                for (int i = 0; i < fillColors.size(); i++) {

                    int tempFillColor = (int) fillColors.get(i);

                    int i1 = 0;
                    while (i1 < colors.size()) {
                        int tempColors = (int) colors.get(i1);
                        if (tempFillColor != tempColors & !fillColors.contains(tempColors) & tempColors != currentColor) {
                            fillColors.set(i, tempColors);
                        }
                        i1++;
                    }
                }
                view1.setBackgroundColor(fillColors.get(1));
                view2.setBackgroundColor(fillColors.get(2));
                view3.setBackgroundColor(fillColors.get(3));
            }
        });


    }

}


